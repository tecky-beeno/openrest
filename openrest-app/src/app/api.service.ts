import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  apiOrigin = 'http://localhost:8100'

  constructor() {}

  async get(url: string) {
    let res = await fetch(this.apiOrigin + url)
    let json = await res.json()
    return json
  }
}
