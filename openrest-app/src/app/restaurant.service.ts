import { Injectable } from '@angular/core'
import { ApiService } from './api.service'

@Injectable({
  providedIn: 'root',
})
export class RestaurantService {
  constructor(private api: ApiService) {}

  getRestaurantList(): Promise<
    Array<{
      id: number
      name: string
      reviews: Array<{
        user_id: number
        nickname: string
        content: string
      }>
    }>
  > {
    return this.api.get('/restaurant')
  }
}
