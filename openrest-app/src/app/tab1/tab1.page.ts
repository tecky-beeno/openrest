import { Component } from '@angular/core'
import { RestaurantService } from '../restaurant.service'

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {
  list$ = this.restaurantService.getRestaurantList()

  constructor(private restaurantService: RestaurantService) {}
}
