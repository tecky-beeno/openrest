import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

  if (!(await knex.schema.hasTable('restaurant'))) {
    await knex.schema.createTable('restaurant', table => {
      table.increments('id')
      table.text('name').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('user'))) {
    await knex.schema.createTable('user', table => {
      table.increments('id')
      table.text('nickname').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('review'))) {
    await knex.schema.createTable('review', table => {
      table.increments('id')
      table.integer('restaurant_id').unsigned().notNullable().references('restaurant.id')
      table.integer('user_id').unsigned().notNullable().references('user.id')
      table.text('content').notNullable()
      table.timestamps(false, true)
    })
  }
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('review')
  await knex.schema.dropTableIfExists('user')
  await knex.schema.dropTableIfExists('restaurant')
}
