import { filter } from 'better-sqlite3-proxy'
import express from 'express'
import cors from 'cors'
import { print } from 'listening-on'
import { proxy } from './proxy'
import { db } from './db'

let app = express()

app.use(express.static('public'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use(cors())

let select_restaurant = db.prepare(/* sql */ `
select id, name
from restaurant
order by id desc
limit 20
	`)

let select_review = db.prepare(/* sql */ `
select
  user_id
, nickname
, content
from review
inner join user on user.id = review.user_id
where restaurant_id = ?
`)

app.get('/restaurant', (req, res) => {
  console.log('get restaurant list')

  let rows = select_restaurant.all()
  console.log('found rows:', rows)
  res.json(
    rows.map(rest => {
      return {
        id: rest.id,
        name: rest.name,
        reviews: select_review.all(rest.id),
      }
    }),
  )

  // res.json(
  //   proxy.restaurant
  //     // .slice(0, 20)
  //     .map(rest => {
  //       return {
  //         id: rest.id,
  //         name: rest.name,
  //         reviews: filter(proxy.review, { restaurant_id: rest.id! }).map(
  //           review => {
  //             return {
  //               user_id: review.user_id,
  //               nickanme: review.user?.nickname,
  //               content: review.content,
  //             }
  //           },
  //         ),
  //       }
  //     }),
  // )
})

let port = 8100
app.listen(port, () => {
  print(port)
})
