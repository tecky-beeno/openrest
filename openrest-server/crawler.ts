import 'playwright'
import { chromium, Page } from 'playwright'
import { proxy } from './proxy'

async function main() {
  let browser = await chromium.launch({ headless: false })
  let page = await browser.newPage()
  let collector = new Collector(page)
  await collector.collectByDistrict('tsuen-wan')
}

class Collector {
  constructor(private page: Page) {}

  async collectByDistrict(district: string) {
    let { page } = this
    let url =
      `https://www.openrice.com/en/hongkong/restaurants/district/` + district
    await page.goto(url, { waitUntil: 'domcontentloaded' })
    let items = await page.evaluate(() => {
      return Array.from(
        document.querySelectorAll(
          '#or-route-poi-list-main ul.pois-restaurant-list > li',
        ),
        li => {
          console.log('li:', li)
          let name = li.querySelector('.title-name')?.textContent?.trim()
          if (!name) throw new Error('failed to find restaurant name')
          let url = li.querySelector<HTMLAnchorElement>('.title-name a')?.href
          if (!url) throw new Error('failed to find restaurant url')
          return { name, url }
        },
      )
    })
    for (let item of items) {
      let restaurant_id = +item.url.match(/-r(\d+)$/)?.[1]!
      if (!restaurant_id) throw new Error('failed to parse restaurant id')
      proxy.restaurant[restaurant_id] = { name: item.name }
      let url = item.url + '/reviews'
      await page.goto(url, { waitUntil: 'domcontentloaded' })
      let reviews = await page.evaluate(() => {
        return Promise.all(
          Array.from(
            document.querySelectorAll<HTMLDivElement>(
              '[data-review-id][itemprop=review]',
            ),
            async div => {
              let id = +div.dataset.reviewId!
              if (!id) throw new Error('failed to find review id')
              let a = div.querySelector<HTMLAnchorElement>(
                '[itemprop=author] a',
              )
              if (!a) throw new Error('failed to find author')
              let user_id = +new URLSearchParams(new URL(a.href).search).get(
                'userid',
              )!
              if (!user_id) throw new Error('failed to reviewer user id')
              let nickname = a.querySelector('[itemprop=name]')?.textContent
              if (!nickname) throw new Error('failed to reviewer nickname')
              let _readMoreBtn =
                div.querySelector<HTMLDivElement>('.read-more-btn')
              if (!_readMoreBtn) throw new Error('failed to find read-more-btn')
              let readMoreBtn = _readMoreBtn
              readMoreBtn.click()
              await new Promise<void>(resolve => {
                function check() {
                  if (getComputedStyle(readMoreBtn).width != 'auto') {
                    resolve()
                  } else {
                    setTimeout(check, 500)
                  }
                }
                check()
              })
              div.querySelector('.review-container .info-wrapper')?.remove()
              let reviewContent =
                div.querySelector<HTMLDivElement>(
                  '.review-container',
                )?.innerText
              if (!reviewContent)
                throw new Error('failed to find review content')
              return { id, user_id, nickname, reviewContent }
            },
          ),
        )
      })
      for (let review of reviews) {
        proxy.user[review.user_id] = { nickname: review.nickname }
        proxy.review[review.id] = {
          restaurant_id,
          user_id: review.user_id,
          content: review.reviewContent,
        }
      }
    }
  }
}

main().catch(e => console.error(e))
