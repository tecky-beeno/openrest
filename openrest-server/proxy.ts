import { proxySchema } from 'better-sqlite3-proxy'
import { db } from './db'

export type Restaurant = {
  id?: number | null
  name: string
}

export type User = {
  id?: number | null
  nickname: string
}

export type Review = {
  id?: number | null
  restaurant_id: number
  restaurant?: Restaurant
  user_id: number
  user?: User
  content: string
}

export type DBProxy = {
  restaurant: Restaurant[]
  user: User[]
  review: Review[]
}

export let proxy = proxySchema<DBProxy>({
  db,
  tableFields: {
    restaurant: [],
    user: [],
    review: [
      /* foreign references */
      ['restaurant', { field: 'restaurant_id', table: 'restaurant' }],
      ['user', { field: 'user_id', table: 'user' }],
    ],
  },
})
